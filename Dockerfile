# This is tomcat image 

FROM tomcat

MAINTAINER s.v.siva.kumar.ponnaganti@ericsson.com

ARG deployable_artifact

COPY $deployable_artifact /usr/local/tomcat/webapps/

CMD ["catalina.sh", "run"]